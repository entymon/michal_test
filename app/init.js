michalWeb = angular.module('michalWeb', [
        'ui.router'
    ])

.run(function() {

})

.config(function($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

    // setup an abstract state for the tabs directive
    .state('home', {
        url: '/',
        views: {
            '': {
                templateUrl: 'app/Templates/Home.tpl',
                controller: 'Controller/HomeCtrl'
            }
        }
    })

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/');

})

.constant('config', {
  'api_url':'db.json'
});
